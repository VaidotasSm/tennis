package lt.vaidotas.sm.tennis.game;

import lt.vaidotas.sm.tennis.game.api.Player;
import lt.vaidotas.sm.tennis.game.api.Score;
import lt.vaidotas.sm.tennis.game.api.TennisGame;
import lt.vaidotas.sm.tennis.game.impl.DefaultTennisGame;
import org.junit.Test;

import java.util.function.Consumer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class DefaultTennisGameTest {

    private final Player player1 = new Player("P1");
    private final Player player2 = new Player("P2");

    @Test
    public void initialScoreShouldBeLove() {
        TennisGame game = new DefaultTennisGame("Game").addPlayer(player1).addPlayer(player2);
        game.start();

        assertThat(game.getScore(player1), equalTo(Score.LOVE));
        assertThat(game.getScore(player2), equalTo(Score.LOVE));
    }


    @Test
    public void canIncrementScore() {
        TennisGame game = new DefaultTennisGame("Game").addPlayer(player1).addPlayer(player2);
        game.start();

        game.score(player1);
        assertThat(game.getScore(player1), equalTo(Score.FIFTEEN));
        assertThat(game.getScore(player2), equalTo(Score.LOVE));

        game.score(player1);
        assertThat(game.getScore(player1), equalTo(Score.THIRTY));
        assertThat(game.getScore(player2), equalTo(Score.LOVE));

        game.score(player2);
        assertThat(game.getScore(player1), equalTo(Score.THIRTY));
        assertThat(game.getScore(player2), equalTo(Score.FIFTEEN));

        game.score(player1);
        assertThat(game.getScore(player1), equalTo(Score.FORTY));
        assertThat(game.getScore(player2), equalTo(Score.FIFTEEN));

        game.score(player2);
        assertThat(game.getScore(player1), equalTo(Score.FORTY));
        assertThat(game.getScore(player2), equalTo(Score.THIRTY));

        game.score(player2);
        assertThat(game.getScore(player1), equalTo(Score.FORTY));
        assertThat(game.getScore(player2), equalTo(Score.FORTY));

        game.score(player1);
        assertThat(game.getScore(player1), equalTo(Score.ADVANTAGE));
        assertThat(game.getScore(player2), equalTo(Score.FORTY));

        game.score(player2);
        assertThat(game.getScore(player1), equalTo(Score.DEUCE));
        assertThat(game.getScore(player2), equalTo(Score.DEUCE));

        game.score(player1);
        assertThat(game.getScore(player1), equalTo(Score.ADVANTAGE));
        assertThat(game.getScore(player2), equalTo(Score.DISADVANTAGE));
    }


    @Test
    public void shouldFinishOn4ToZero() {
        TennisGame game = new DefaultTennisGame("Game").addPlayer(player1).addPlayer(player2);
        game.start();

        game.score(player2);
        game.score(player2);
        game.score(player2);
        game.score(player2);

        assertThat(game.isFinished(), is(true));
    }


    @Test
    public void doesNotFinishGameWithoutTwoPointLead() {
        TennisGame game = new DefaultTennisGame("Game")
                .addPlayer(player1)
                .addPlayer(player2)
                .start();

        game
                .score(player2)
                .score(player2)
                .score(player1)
                .score(player2);

        assertThat(game.isFinished(), is(false));
    }

    @Test
    public void scoreListenerShouldBeInvokedOnScore() {
        Consumer<Player> listener = mock(Consumer.class);
        TennisGame game = new DefaultTennisGame("Game")
                .addPlayer(player1)
                .addPlayer(player2)
                .onScore(listener)
                .start();

        game.score(player1);

        verify(listener, times(1)).accept(eq(player1));
    }

    @Test
    public void winListenerShouldBeInvokedOnWonGame() {
        Consumer<Player> listener = mock(Consumer.class);
        TennisGame game = new DefaultTennisGame("Game")
                .addPlayer(player1)
                .addPlayer(player2)
                .onWin(listener)
                .start();

        game
                .score(player1)
                .score(player1)
                .score(player1)
                .score(player1);

        verify(listener, times(1)).accept(eq(player1));
    }

}