package lt.vaidotas.sm.tennis.game;

import lt.vaidotas.sm.tennis.game.api.Player;
import lt.vaidotas.sm.tennis.game.api.TennisGame;
import lt.vaidotas.sm.tennis.game.impl.DefaultTennisGame;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class DefaultTennisGameEdgeCasesTestTest {

    private final Player player1 = new Player("P1");
    private final Player player2 = new Player("P2");

    @Test(expected = NullPointerException.class)
    public void nameCannotBeNull() {
        new DefaultTennisGame(null);
    }

    @Test(expected = NullPointerException.class)
    public void cannotCreateGameWithoutPlayers() {
        new DefaultTennisGame("Game1")
                .addPlayer(null);
    }

    @Test(expected = IllegalStateException.class)
    public void cannotAddTooManyPlayers() {
        new DefaultTennisGame("Game1")
                .addPlayer(player1)
                .addPlayer(player2)
                .addPlayer(new Player("P3"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotAddSamePlayer() {
        new DefaultTennisGame("Game1")
                .addPlayer(player1)
                .addPlayer(player1);
    }

    @Test(expected = IllegalStateException.class)
    public void notStartedGameHasNoScore() {
        new DefaultTennisGame("Game")
                .addPlayer(player1)
                .addPlayer(player2)
                .getScore(player1);
    }

    @Test
    public void cannotStartGameWithoutTwoPlayers() {
        TennisGame game = new DefaultTennisGame("Game");

        assertHasException(() -> game.start(), IllegalStateException.class);

        game.addPlayer(player1);
        assertHasException(() -> game.start(), IllegalStateException.class);
    }

    @Test
    public void notStartedGameApiCannotBeUsed() {
        TennisGame game = new DefaultTennisGame("Game").addPlayer(player1).addPlayer(player2);

        assertHasException(() -> game.getScore(player1), IllegalStateException.class);
        assertHasException(() -> game.score(player1), IllegalStateException.class);
    }

    @Test
    public void cannotUseNonParticipatingPlayer() {
        Player spectator = new Player("Spectator");
        TennisGame game = new DefaultTennisGame("Game").addPlayer(player1).addPlayer(player2);
        game.start();

        assertHasException(() -> game.getScore(spectator), IllegalArgumentException.class);
        assertHasException(() -> game.score(spectator), IllegalArgumentException.class);
    }

    @Test(expected = IllegalStateException.class)
    public void cannotStartGameTwice() {
        TennisGame game = new DefaultTennisGame("Game").addPlayer(player1).addPlayer(player2);
        game.start();
        game.start();
    }

    @Test(expected = IllegalStateException.class)
    public void cannotUseFinishedGame() {
        TennisGame game = new DefaultTennisGame("Game").addPlayer(player1).addPlayer(player2);
        game.start();
        game.score(player2);
        game.score(player2);
        game.score(player2);
        game.score(player2);

        game.score(player2);
    }

    private <E extends Class<?>> void assertHasException(Runnable task, E exceptionClass) {
        try {
            task.run();
            fail("Expected to throw IllegalStateException");
        } catch (Throwable t) {
            assertThat(t, instanceOf(exceptionClass));
        }
    }

}
