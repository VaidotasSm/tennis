package lt.vaidotas.sm.tennis;

import lt.vaidotas.sm.tennis.game.api.Player;
import lt.vaidotas.sm.tennis.game.api.TennisGame;
import lt.vaidotas.sm.tennis.game.impl.DefaultTennisGame;

import java.util.Scanner;

public class TennisLibraryDemoApp implements Runnable {

    @Override
    public void run() {
        System.out.println("--- Tennis Game ---");
        printHelp();
        System.out.println("-------------------");

        TennisGame game = null;
        Scanner scanner = new Scanner(System.in);
        String input;
        String[] params;
        while (true) {
            input = scanner.nextLine();
            try {
                if (input.startsWith("help")) {
                    printHelp();
                } else if (input.startsWith("abort")) {
                    System.out.println("Aborting game...");
                    System.exit(0);
                } else if (input.startsWith("game")) {
                    params = toTwoParams(input, "Name must be specified");
                    game = new DefaultTennisGame(params[1]);
                    addListeners(game);

                } else if (input.startsWith("add")) {
                    requireGameReady(game);
                    params = toTwoParams(input, "Player name must be specified");
                    game.addPlayer(new Player(params[1]));
                } else if (input.startsWith("start")) {
                    requireGameReady(game);
                    game.start();
                } else if (input.startsWith("score")) {
                    requireGameReady(game);
                    params = toTwoParams(input, "Player name must be specified");
                    game.score(new Player(params[1]));
                } else {
                    printError("Unknown command: type 'help' for available commands");
                }
            } catch (Exception ex) {
                printError(ex.getMessage());
            }
        }
    }

    private void addListeners(TennisGame game) {
        game.
                onScore(p -> {
                    System.out.println(String.format("%s scored a point", p.getName()));
                    printScores(game);
                })
                .onWin(p -> {
                    System.out.println(String.format("~~~ Game '%s' is finished ~~~", game.getName()));
                    System.out.println(String.format("%s won the game", p.getName()));
                    System.exit(0);
                });
    }

    private void printScores(TennisGame game) {
        game.getPlayers().forEach(player -> {
            System.out.println(String.format("%s: %s", player.getName(), game.getScore(player).getValue()));
        });
    }

    private void printError(String message) {
        System.out.println("~~~ Error ~~~");
        System.out.println(message);
        System.out.println("~~~~~~");
    }

    private void printHelp() {
        System.out.println("Available commands (examples):");
        System.out.println("help            - see available commands");
        System.out.println("abort           - exit application");
        System.out.println("game Game1      - set game name");
        System.out.println("add Player1     - add player");
        System.out.println("start           - start game");
        System.out.println("score Player1   - score point for specific player");
    }

    private String[] toTwoParams(String input, String errorMessage) throws Exception {
        String[] params = input.split(" ");
        if (params.length < 2) {
            throw new Exception(errorMessage);
        }
        return params;
    }

    private void requireGameReady(TennisGame game) {
        if (game == null) {
            throw new NullPointerException("Make sure game is ready - name set, players added, started");
        }
    }

}
