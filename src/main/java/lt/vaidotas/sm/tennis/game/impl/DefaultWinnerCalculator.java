package lt.vaidotas.sm.tennis.game.impl;

import lt.vaidotas.sm.tennis.game.api.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DefaultWinnerCalculator implements WinnerCalculator {

    public boolean isScoreWon(Map<Player, Integer> score) {
        List<Integer> scoreValues = new ArrayList<>(score.values());
        Integer first = scoreValues.get(0);
        Integer second = scoreValues.get(1);

        return (reachedMinimumToWin(first) && hasTwoPointLead(first, second)) ||
                (reachedMinimumToWin(second) && hasTwoPointLead(second, first));
    }

    private boolean hasTwoPointLead(Integer first, Integer second) {
        return first - PlayerScore.SCORE_REQUIRED_LEAD_TO_WIN >= second;
    }

    private boolean reachedMinimumToWin(Integer first) {
        return first >= PlayerScore.SCORE_MIN_TO_WIN;
    }

}
