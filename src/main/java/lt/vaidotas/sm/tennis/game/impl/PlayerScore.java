package lt.vaidotas.sm.tennis.game.impl;

import lt.vaidotas.sm.tennis.game.api.Player;

class PlayerScore {

    public static final int SCORE_MIN_TO_WIN = 4;
    public static final int SCORE_REQUIRED_LEAD_TO_WIN = 2;

    private final Player player;
    private int score;

    public PlayerScore(Player player) {
        this.player = player;
    }

    public void increase() {
        score++;
    }

    public Player getPlayer() {
        return player;
    }

    public int getScore() {
        return score;
    }
}
