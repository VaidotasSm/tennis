package lt.vaidotas.sm.tennis.game.impl;

import lt.vaidotas.sm.tennis.game.api.Player;
import lt.vaidotas.sm.tennis.game.api.Score;
import lt.vaidotas.sm.tennis.game.api.TennisGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

public class DefaultTennisGame implements TennisGame {

    private final WinnerCalculator calculator;

    private final String name;
    private final Map<Player, Integer> score = new HashMap<>();
    private final List<Player> playerOrder = new ArrayList<>();
    private GameStatus status = GameStatus.NOT_STARTED;

    private final List<Consumer<Player>> scoreListeners = new ArrayList<>();
    private final List<Consumer<Player>> winListeners = new ArrayList<>();

    public DefaultTennisGame(String name, WinnerCalculator calculator) {
        requireNonNull(name);
        requireNonNull(calculator);

        this.name = name;
        this.calculator = calculator;
    }

    public DefaultTennisGame(String name) {
        requireNonNull(name);
        this.name = name;
        calculator = new DefaultWinnerCalculator();
    }

    @Override
    public TennisGame addPlayer(Player player) {
        requireNonNull(player);
        if (score.containsKey(player)) {
            throw new IllegalArgumentException("Player already exists");
        }
        if (score.size() >= 2) {
            throw new IllegalStateException("Too many players already");
        }
        score.put(player, 0);
        playerOrder.add(player);
        return this;
    }

    @Override
    public TennisGame onScore(Consumer<Player> callback) {
        requireNonNull(callback);
        scoreListeners.add(callback);
        return this;
    }

    @Override
    public TennisGame onWin(Consumer<Player> callback) {
        requireNonNull(callback);
        winListeners.add(callback);
        return this;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isStarted() {
        return status == GameStatus.IN_PROGRESS;
    }

    @Override
    public boolean isFinished() {
        return status == GameStatus.FINISHED;
    }

    @Override
    public TennisGame start() {
        requireStatusAnyOf(GameStatus.NOT_STARTED);
        requireTwoPlayers();
        status = GameStatus.IN_PROGRESS;
        return this;
    }

    @Override
    public TennisGame score(Player player) {
        requireStatusAnyOf(GameStatus.IN_PROGRESS);
        requirePlayer(player);
        score.put(player, score.get(player) + 1);

        scoreListeners.forEach(l -> l.accept(player));
        handleWin(player);
        return this;
    }

    @Override
    public Score getScore(Player player) {
        requireStatusAnyOf(GameStatus.IN_PROGRESS, GameStatus.FINISHED);
        requirePlayer(player);
        Integer otherScore = score.entrySet().stream()
                .filter(e -> e.getKey() != player)
                .findAny().orElseThrow(() -> new IllegalStateException("There are no other player"))
                .getValue();
        return Score.of(score.get(player), otherScore);
    }

    @Override
    public List<Player> getPlayers() {
        return new ArrayList<>(playerOrder);
    }

    private void handleWin(Player player) {
        if (calculator.isScoreWon(score)) {
            status = GameStatus.FINISHED;
            winListeners.forEach(l -> l.accept(player));
        }
    }

    private void requireStatusAnyOf(GameStatus... requiredStatuses) {
        if (Stream.of(requiredStatuses).noneMatch(s -> s == status)) {
            throw new IllegalStateException("Game must be started");
        }
    }

    private void requirePlayer(Player player) {
        if (!score.containsKey(player)) {
            throw new IllegalArgumentException(String.format("Player %s does not participate in game", player.getName()));
        }
    }

    private void requireTwoPlayers() {
        if (score.size() != 2) {
            throw new IllegalStateException("Not enough players to start game");
        }
    }

}
