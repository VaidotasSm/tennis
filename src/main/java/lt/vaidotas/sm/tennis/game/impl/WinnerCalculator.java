package lt.vaidotas.sm.tennis.game.impl;

import lt.vaidotas.sm.tennis.game.api.Player;

import java.util.Map;

public interface WinnerCalculator {
    boolean isScoreWon(Map<Player, Integer> score);
}
