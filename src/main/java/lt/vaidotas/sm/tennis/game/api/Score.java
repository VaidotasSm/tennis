package lt.vaidotas.sm.tennis.game.api;

public enum Score {
    LOVE("love"),
    FIFTEEN("fifteen"),
    THIRTY("thirty"),
    FORTY("forty"),
    DEUCE("deuce"),
    ADVANTAGE("advantage"),
    DISADVANTAGE("disadvantage");

    public static Score of(int targetScore, int otherScore) {
        if (targetScore < 0) {
            throw new IllegalArgumentException("Score should never be bellow zero");
        }

        switch (targetScore) {
            case 0:
                return Score.LOVE;
            case 1:
                return Score.FIFTEEN;
            case 2:
                return Score.THIRTY;
            case 3:
                return Score.FORTY;
            default:
                if (targetScore == otherScore) {
                    return Score.DEUCE;
                }
                return targetScore > otherScore ? ADVANTAGE : DISADVANTAGE;
        }
    }

    private final String value;

    Score(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
