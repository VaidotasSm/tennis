package lt.vaidotas.sm.tennis.game.impl;

enum GameStatus {
    NOT_STARTED,
    IN_PROGRESS,
    FINISHED
}
