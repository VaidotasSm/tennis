package lt.vaidotas.sm.tennis.game.api;

import java.util.List;
import java.util.function.Consumer;

public interface TennisGame {

    boolean isStarted();

    boolean isFinished();

    TennisGame addPlayer(Player player);

    TennisGame onScore(Consumer<Player> callback);

    TennisGame onWin(Consumer<Player> callback);

    String getName();

    TennisGame start();

    TennisGame score(Player player);

    Score getScore(Player player);

    List<Player> getPlayers();

}
