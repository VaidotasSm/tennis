# Tennis Game


## Prerequisites

* `Java SDK 1.8+`

## Instructions

1. Build: `gradlew clean build`
2. Run: `java -jar build/libs/lt.vaidotas.sm.tennis-0.0.1.jar` (from project root in terminal).
3. Follow instructions on terminal screen.